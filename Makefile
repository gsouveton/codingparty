################################ PHONIES ################################

.PHONY: clean, all
################################ FOR DEBUG ################################

MAIN = main

################################ OPTIONS ################################

CC = gcc
CFLAGS = -Wall
BUILD = build/main

#if [ -d build/init ]


################################ OBJECTS ################################

OBJS = 

################################ BUILDER ################################

all : init cook customer waiter
	cp build/main ./init

init : build/obj/main.o build/obj/common.o
	$(CC) -o build/main build/obj/main.o build/obj/common.o

cook : build/obj/cook_main.o build/obj/common.o
	$(CC) -o build/cook build/obj/cook_main.o build/obj/common.o

customer : build/obj/customer_main.o build/obj/common.o
	$(CC) -o build/customer build/obj/customer_main.o build/obj/common.o

waiter : build/obj/waiter_main.o build/obj/common.o
	$(CC) -o build/waiter build/obj/waiter_main.o build/obj/common.o

################################ COMMON PART ################################

build/obj/common.o : src/common.c src/global.h
	$(CC) -c src/common.c -o build/obj/common.o $(CFLAGS)

################################ MAIN PART ################################

build/obj/main.o : src/init/main.c src/global.h
	$(CC) -c src/init/main.c -o build/obj/main.o $(CFLAGS)

build/obj/cook_main.o : src/cook/main.c src/global.h
	$(CC) -c src/cook/main.c -o build/obj/cook_main.o $(CFLAGS)

build/obj/customer_main.o : src/customer/main.c src/global.h
	$(CC) -c src/customer/main.c -o build/obj/customer_main.o $(CFLAGS)

build/obj/waiter_main.o : src/waiter/main.c src/global.h
	$(CC) -c src/waiter/main.c -o build/obj/waiter_main.o $(CFLAGS)

################################ CLEANER ################################

clean :
	rm -r build/obj/*.o 2>/dev/null
	find build/ -executable -type f -delete
	find . -maxdepth 1 -type f -name "init" -delete