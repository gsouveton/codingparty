#include "cook.h"

cook_t *me = NULL; /* globale car utilisée dans at_exit et surement aussi dans les IRQ */

int main ( int argc , char * argv [ ] )
{
        /* Variables */

        FILE        *key_file;
        key_t        key_code;
        int          semid;
        int          msgqid;

        /* Init */

        atexit     ( kill_the_cook );  /* We want the cook to be cleanly free'd on exit */
        ssigaction ( SIGUSR1 , exit ); /* SIGUSR1 means clean exit*/

        me = newcook ( atoi ( argv [ 1 ] ) , atoi ( argv [ 2 ] ) );

        fprintf ( stderr , "[cook %d] Notice : Hi !\n" , me -> id );

        /* Get shared ressources */

        key_file = NULL;
        key_file = fopen ( KEY_FILE , "r" );
        if ( ! key_file )
        {
            fprintf ( stderr , "[cook %d] Error : The file needed to compute the shm key doesn't exist !\n" , me -> id );
            exit ( -1 );
        }
        fclose ( key_file );

        key_code = ftok ( KEY_FILE , getppid ( ) );

        semid = semget ( key_code , 1 + me -> nbtools , 0 );
        if ( semid < 0 ) 
        {
            fprintf ( stderr , "[cook %d] Error : semget returned -1 !\n" , me -> id );
            exit ( -1 );
        }
        msgqid = msgget ( key_code , 0 );
        if ( msgqid < 0 ) 
        {
            fprintf ( stderr , "[cook %d] Error : msgget returned -1 !\n" , me -> id );
            exit ( -1 );
        }        

        /* Main loop */

        while ( 1 )
        {
                fprintf  ( stderr , "[cook %d] Notice : Waiting.\n" , me -> id );
                getOrder ( me , msgqid );
                fprintf  ( stderr , "[cook %d] Notice : Got something to cook !\n" , me -> id );
                getTools ( me , semid );
                fprintf  ( stderr , "[cook %d] Notice : I finally got my hammer !\n" , me -> id );
                doIt     ( me );
                //fprintf ( stderr , "[cook %d] Notice : hmmm, that smells so good !\n",me->id);
                giveToolsBack ( me , semid );
                //fprintf ( stderr , "[cook %d] Notice : The hammer is back at its place !\n",me->id);
                tellOrderIsReady ( msgqid , me );
                //fprintf ( stderr , "[cood %d] Notice : Chitchat with the waiter finished !\n",me->id);
                clean_the_kitchen ( me );
        }  
        exit ( 0 );
}

/*
        Maybe a bit overkill to dynamicly allocate the memory for one static struct, still here because we used to
        put a int[] with a dynamic length
*/

cook_t * newcook ( int id, int nbtools )
{
        cook_t *  out = malloc ( sizeof ( cook_t ) );
        out -> id = id;
        out -> nbtools = nbtools;
        out -> task = NULL;
        return out;        
}

/*
        Makes sure the struct is gently free'd and don't risk any double-free
*/

void kill_the_cook ( void )
{
        fprintf ( stderr , "[cook %d] Notice : I got fired :'( !\n" , me -> id );
        if ( me != NULL )
        {
                if ( me -> task != NULL )
                {
                        free ( me -> task -> neededtools );
                                        me -> task -> neededtools = NULL;
                        free ( me -> task );
                                        me -> task = NULL;
                }
                free ( me );
                        me = NULL;
        }
}

void getTools ( cook_t * me, int semid )
{
        struct sembuf * sops = calloc ( sizeof ( struct sembuf ) , 2 + me -> nbtools );
        int      i = 0;
        int offset = 0;
        
        #if 0
		        if ( semctl ( semid , 0 , GETVAL ) )
                        fprintf( stderr, "[Cook %d] Notice : unlocked, proceding\n" , me -> id );
                else
                        fprintf( stderr, "[Cook %d] Notice : locked, waiting\n" , me -> id );
        #endif

        /* Wait for the toolbox to be available (no concurrent `take` access) */

        sops [ 0 ] . sem_num = 0;   /* the 'mutex' */
        sops [ 0 ] . sem_op = -1;   /* only one access can be done at the same time */
        sops [ 0 ] . sem_flg = 0;

        /* Get every tools we need */

        for ( i = 0 ; i < me -> nbtools ; ++i )
        {
                /*fprintf ( stderr , "[Cook %d] Notice : I need %d tools #%d\n", me->id,me->task->neededtools[i], i+1); 
                  fprintf( stderr , "[Cook %d] Notice : %d of them are available \n",me->id,semctl(semid, i+1,GETVAL));*/

				if ( me -> task -> neededtools [ i ] == 0 )
                        offset++;                      /* sem_op = 0 is not to be used :D */
                else
                {
                        sops [ i + 1 - offset ] . sem_num = i + 1; 
                        sops [ i + 1 - offset ] . sem_op = - ( me -> task -> neededtools [ i ] );
                        sops [ i + 1 - offset ] . sem_flg = 0;
                }
        }

        /* Once we finish to take the tools, close the toolbox */

        sops [ me -> nbtools + 1 - offset ] . sem_num = 0;
        sops [ me -> nbtools + 1 - offset ] . sem_op = +1;
        sops [ me -> nbtools + 1 - offset ] . sem_flg = 0;

        semop ( semid , sops, 2 + me -> nbtools - offset );
        free  ( sops );

        #if 0
		        for ( i = 0; i < me -> nbtools ; ++i )
                        fprintf ( stderr , "[Cook %d] Notice : tools #%d, %d are available\n", me->id, i,semctl(semid, i+1,GETVAL));*/
        #endif
}

void giveToolsBack ( cook_t * me , int semid )
{
        struct sembuf * sops = calloc ( sizeof ( struct sembuf ) , me -> nbtools );
        int      i = 0;
        int offset = 0;

        /*
                We don't neeed to lock the toolbox ( and doing it would deadlock every cook ).
        */

        for ( i = 0 ; i < me -> nbtools ; ++i )
        {
                if ( me -> task -> neededtools [ i ] == 0 )
                        offset++;                      /* sem_op = 0 is not to be used :D */
                else
                {
                        sops [ i - offset ] . sem_num = i + 1;
                        sops [ i - offset ] . sem_op = + ( me -> task -> neededtools [ i ] );
                        sops [ i - offset ] . sem_flg = 0;
                }
        }
        semop ( semid , sops , me -> nbtools - offset );
        free ( sops );
}

void doIt ( cook_t * me )
{
        fprintf ( stderr , "[Cook %d] Notice : cooking for %d secs\n" , me -> id , me -> task -> eta );
        sleep ( me -> task -> eta );
}

void tellOrderIsReady ( int msgqid , cook_t * me )
{
        struct msgbuf anchor = { WAITER + me -> task -> higher_pid , { '\0' } };

        msgsnd ( msgqid , & anchor , MAX_MSGQ_LENGTH * sizeof ( char ) , 0 );

        fprintf ( stderr , "[Cook %d] Notice : I finished cooking\n" , me -> id );
}

void clean_the_kitchen ( cook_t * me )
{        
        free ( me -> task -> neededtools );
        me -> task -> neededtools = NULL;
        free ( me -> task );
        me -> task = NULL;
}

void getOrder ( cook_t * me , int msgqid )
{
        struct msgbuf  anchor;
        char          *ptr;

        msgrcv ( msgqid , & anchor , MAX_MSGQ_LENGTH * sizeof ( char ) , ORDER , 0 );

        me -> task = malloc ( sizeof ( order_t ) );                             /* freed in 'clean_the_kitchen' */
        me -> task -> neededtools = malloc ( sizeof ( int ) * me -> nbtools );  /* or in the worst case on exit */

        ptr = anchor . content;  /* ptr is a cursor on anchor.content*/

        memcpy ( & ( me -> task -> higher_pid ) , ptr , sizeof ( int ) );  /*copy from cursor in anchor.content */

        ptr += sizeof ( int );    /* shifts the cursor */

        memcpy ( & ( me -> task -> eta) , ptr , sizeof ( unsigned int ) ); /*copy from cursor in anchor.content */

        ptr += sizeof ( unsigned int );    /* shifts the cursor */

        memcpy ( me -> task -> neededtools , ptr , sizeof ( int ) * me -> nbtools ); /*copy from cursor in anchor.content */
}
