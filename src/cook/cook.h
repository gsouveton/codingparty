#ifndef _COOK_H_
#define _COOK_H_

#include "../global.h"

        /* Creates a new cook */
        cook_t * newcook ( int , int );

        /* Kills the cook ( frees allocated memory ) */
        void kill_the_cook ( void );

        /* Get an order from the message queue */
        void getOrder ( cook_t * , int );

        /* Get the tools using the semaphores */
        void getTools ( cook_t * , int );

        /* Bake ( sleep ) */
        void doIt ( cook_t * );

        /* Put tools back in the semaphores */
        void giveToolsBack ( cook_t * , int );

        /* Tells the waiter his order is ready */
        void tellOrderIsReady ( int , cook_t * );

        /* Make some frees */
        void clean_the_kitchen ( cook_t * );

#endif
