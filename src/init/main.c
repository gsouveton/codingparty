#include "../global.h"

// Must be global to grant 'cleaning' an access to them.

char         loop;

int          semid_tools;
int          semid_waiters;
int          semid_terms;

pid_t       *waiters;
pid_t       *cooks;
pid_lt      *customers;
tool_t      *tools;

char        *_cookInstanceName;
char        *_waiterInstanceName;
char        *_customerInstanceName;

char       **_cookInstanceArgs;
char       **_waiterInstanceArgs;
char       **_customerInstanceArgs;

int          shm_fastfood;
fastfood_t  *gfastfood;

int          msgq;

fastfood_t   fastfood;


int nb_client = 0;

void sigchld_handler(int sig)
{
	pid_t son;
	while ( (son = waitpid(-1,NULL,WNOHANG)) > 0 )
	{
		nb_client++;
		customers = pidl_rm ( customers , son );
	}
}


int stoploop ( void )
{
        loop = 0;
        raise ( SIGKILL );
        return 0;
}

void cleaning ( void )
{
        int i;
          pid_lt * ptr_custo = customers;
        /*int * status= NULL;*/

        fprintf ( stderr , "Closing the fastfood ...\n" );

        fprintf ( stderr , "Fireing the cooks ...\n" );
        for ( i = 0 ; i < fastfood . cooks_c ; i++ )
        {
                kill ( cooks [ i ] , SIGUSR1 );
        }
        fprintf ( stderr , "Fireing the waiters ...\n" );
        for ( i = 0 ; i < fastfood . waiters_c ; i++ )
        {
                kill ( waiters [ i ] , SIGUSR1 );
        }
        fprintf ( stderr , "Kicking the customers out ...\n" );
        while ( ptr_custo != NULL )
        {
                kill (ptr_custo->pid , SIGUSR1);
                ptr_custo = ptr_custo->next;
        }                
        
        fprintf ( stderr , "Waiting for everyone to get out ...\n" );
        while ( wait(NULL) > 0 );
        
        fprintf ( stderr , "Deleting every kind of organization ...\n" );
        msgctl ( msgq , IPC_RMID , NULL );
        semctl ( semid_tools , 0 , IPC_RMID );
        semctl ( semid_waiters , 0 , IPC_RMID );
        semctl ( semid_terms , 0 , IPC_RMID );
        shmdt  ( gfastfood );
        shmctl ( shm_fastfood , IPC_RMID , NULL );

                fprintf ( stderr , "Everything's clear ...\n" );
        ffree ( waiters );
        ffree ( cooks );
                ffree ( tools );
                lfree ( customers );
                ffree ( _cookInstanceName );
        ffree ( _cookInstanceArgs [ 0 ] );
        ffree ( _cookInstanceArgs [ 1 ] );
        ffree ( _cookInstanceArgs [ 2 ] );
                ffree ( _cookInstanceArgs );
        ffree ( _waiterInstanceName );
        ffree ( _waiterInstanceArgs [ 0 ] );
        ffree ( _waiterInstanceArgs [ 1 ] );
                ffree ( _waiterInstanceArgs );
        ffree ( _customerInstanceName );
                for ( i = 0 ; i <= fastfood . tools_c + 1 ; i++ )
                ffree ( _customerInstanceArgs [ i ] );
        ffree(_customerInstanceArgs);
        return;
}


int main ( int argc , char * argv [ ] , char * envp [ ] )
{
        /* Variables declaration */

                int       i;
        int       r;
        FILE      *key_file;
        
        key_t     ipckey;
        key_t     ipckey_terms;
        key_t     ipckey_tools;
        key_t     ipckey_waiters;
        pid_t     me;
        pid_t     tmppid;

        int       tmpcooktime;
        int       tmptools_c;
		int       cusnum = 0;

        /* Argument count control. */

        if ( argc < 4 )
        {
                fprintf ( stderr , "%d arguments missing\n" , 4 - argc );
                exit ( -1 );
        }

        fprintf ( stdout , "The restaurant manager launches a recruitement notice ...\n" );

        /* Getting parameters and testing them. */

        fastfood . waiters_c  = atoi ( argv [ 1 ] );
        nb_client = 4 * fastfood . waiters_c;
        fastfood . cooks_c    = atoi ( argv [ 2 ] );
        fastfood . terms_c    = atoi ( argv [ 3 ] );
        fastfood . tools_c    = argc - 4;

        if ( fastfood . cooks_c < 1 )
        {
                fprintf ( stderr , "Error : You must specify a positive number of cooks.\n" );
                exit ( -1 );
        }

        if ( fastfood . terms_c < 1 )
        {
                fprintf ( stderr , "Error : You must specify a positive number of terms.\n" );
                exit ( -1 );
        }

        if ( fastfood . waiters_c < 1 )
        {
                fprintf ( stderr , "Error : You must specify a positive number of waiters.\n" );
                exit ( -1 );
        }

        /* Initialising variables and allocating memory */

        r                           = 0;
        me                          = getpid ( );

        atexit                                        ( cleaning );
        srand                                         ( ( me * me ) % 65535 );

        waiters                     = malloc          ( fastfood . waiters_c  * sizeof ( pid_t ) );
        cooks                       = malloc          ( fastfood . cooks_c    * sizeof ( pid_t ) );
        tools                       = malloc          ( fastfood . tools_c    * sizeof ( tool_t ) );
        customers                   = pidl_create     ( );

        _cookInstanceName           = getInstancePath ( "cook" );
        _cookInstanceArgs           = malloc          ( sizeof ( char * ) * 4 );
        _cookInstanceArgs [ 0 ]     = malloc          ( sizeof ( char ) * 20 );
        _cookInstanceArgs [ 1 ]     = malloc          ( sizeof ( char ) * 20 );
        _cookInstanceArgs [ 2 ]     = malloc          ( sizeof ( char ) * 20 );

        _waiterInstanceName         = getInstancePath ( "waiter" );
        _waiterInstanceArgs         = malloc          ( sizeof ( char * ) * 3 );
        _waiterInstanceArgs [ 0 ]   = malloc          ( sizeof ( char ) * 20 );
        _waiterInstanceArgs [ 1 ]   = malloc          ( sizeof ( char ) * 20 );

        _customerInstanceName       = getInstancePath ( "customer" );
        _customerInstanceArgs       = malloc          ( sizeof ( char * ) * (fastfood . tools_c + 2) );
        _customerInstanceArgs [ 0 ] = malloc          ( sizeof ( char ) * 20 );

        /* Creating array with tools */
        for ( i = 0 ; i < fastfood . tools_c ; i++ )
                tools [ i ] = atoi ( argv [ i + 4 ] );

        /* Initialising arguments for customers program */
        for ( i = -1 ; i < fastfood . tools_c ; i++ ) // Note : -1 matches with cooktime.
                _customerInstanceArgs [ i + 2 ] = malloc ( sizeof ( char ) * 4 + sizeof ( '\0' ) );

        /* Initialising signal masks. */
        ssigaction ( SIGCHLD , sigchld_handler );
        
        for ( i = 0 ; i < NSIG ; i++ )
        {
                if ( i != SIGCHLD && i != SIGKILL )
                        ssigaction ( i , exit );
        }

        /* Creating file for ftok if not existing */

        key_file = NULL;
        key_file = fopen ( KEY_FILE , "w+" );
        if ( ! key_file )
        {
                fprintf ( stderr , "Error : unable to create key.serv needed to start this program.\n" );
                exit ( -1 );
        }
        fclose(key_file);

        /* Generating keys for ipcs */

                ipckey         = ftok ( KEY_FILE , me );
        ipckey_terms   = ftok ( KEY_FILE , me - 1 );
        ipckey_tools   = ftok ( KEY_FILE , me );
        ipckey_waiters = ftok ( KEY_FILE , me + 1 );

        assert ( ( ipckey + 1 ) && ( ipckey_terms + 1 ) && ( ipckey_tools + 1 ) && ( ipckey_waiters + 1 ) );

        /* Shared memory initialisation. */

        shm_fastfood = shmget ( ipckey , sizeof ( fastfood_t ) , IPC_CREAT | 0660 );
        if ( shm_fastfood == -1 )
        {
                fprintf ( stderr , "Error : unable to shm for fastfood.\n" );
                exit ( -1 );
        }
        gfastfood = shmat ( shm_fastfood , NULL , 0 );
        memcpy ( gfastfood , & fastfood , sizeof ( fastfood_t ) );

        /* Semaphores initialisation. */

        /* Initialising tools' semaphores */

        /* Note : the first semaphore of the set is a lock for the rest. */
                semid_tools = semget ( ipckey_tools , fastfood . tools_c + 1 , IPC_CREAT | 0660 );
        if ( semid_tools == -1 )
        {
                fprintf ( stderr , "Error : unable to create semid_tools.\n" );
                exit ( -1 );
        }
        semctl ( semid_tools , 0 , SETVAL , 1 );
        for ( i = 1 ; i <= fastfood . tools_c ; i++ )
        {
                semctl ( semid_tools , i , SETVAL , tools [ i - 1 ] );
                 fprintf ( stderr , "\tsemaphore tools #%d init at %d\n",i,semctl(semid_tools,i,GETVAL));                                       
        }

        /* Initialising waiters' semaphores */
                semid_waiters = semget ( ipckey_waiters , fastfood . waiters_c + 1 , IPC_CREAT | 0660 );
        if ( semid_waiters == -1 )
        {
                fprintf ( stderr , "Error : unable to create semid_waiters.\n" );
                exit ( -1 );
        }
        semctl ( semid_waiters , 0 , SETVAL , 1 );
        for ( i = 1 ; i <= fastfood . waiters_c ; i++ )
                semctl ( semid_waiters , i , SETVAL , 1 ); /* Available */

        /* Initialising terminals' semaphores */
                semid_terms = semget ( ipckey_terms , 1 , IPC_CREAT | 0660 );
        if ( semid_terms == -1 )
        {
                fprintf ( stderr , "Error : unable to create payement terminals.\n" );
                exit ( -1 );
        }
        semctl ( semid_terms , 0 , SETVAL , fastfood.terms_c );

        /* Message queue initialisation. */
        msgq = msgget ( ipckey , IPC_CREAT | 0660 );
        if ( msgq == -1 )
        {
                fprintf ( stderr , "Error : unable to create message queue.\n" );
                exit ( -1 );
        }

        /* Starting Cooks processes ... */

        fprintf ( stdout , "Recruiting %d cooks ...\n" , fastfood.cooks_c );
        for ( i = 0 ; i < fastfood . cooks_c ; i++ )
        {
                cooks [ i ] = fork ( );
                if ( cooks [ i ] == -1 )
                {
                        fprintf ( stderr , "Error : unable to create cook #%d.\n" , i );
                        exit ( -1 );
                }
                else if ( cooks [ i ] == 0 )
                {
                        sprintf ( _cookInstanceArgs [ 0 ] , "Cook#%d" , i );
                        sprintf ( _cookInstanceArgs [ 1 ] , "%d" , i );
                        sprintf ( _cookInstanceArgs [ 2 ] , "%d" , fastfood . tools_c );
                                  _cookInstanceArgs [ 3 ] = NULL;

                        // /*"./build/" _cookInstanceName*/ "/bin/pause"

                        r = execve ( _cookInstanceName , _cookInstanceArgs , envp );

                        switch ( errno )
                        {
                                case ENOENT: fprintf ( stderr , "Error : %s doesn't exist !\n"                  , _cookInstanceName ); break;
                                case EACCES: fprintf ( stderr , "Error : cannot start %s, permission denied.\n" , _cookInstanceName ); break;
                                case EPERM:  fprintf ( stderr , "Error : %s is not executable !\n"              , _cookInstanceName ); break;
                                case EISDIR: fprintf ( stderr , "Error : %s is a directory !\n"                 , _cookInstanceName ); break;
                                default:     fprintf ( stderr , "Error : %s is unreachable\n"                   , _cookInstanceName ); break;
                        }
                        exit ( -1 ); // Wimp out
                }
        }
        while ( --i );

        /* Starting Waiters processes ... */

        fprintf ( stdout , "Recruiting %d waiters ...\n" , fastfood . waiters_c );
        for ( i = 0 ; i < fastfood . waiters_c ; i++ )
        {
                waiters [ i ] = fork ();
                if ( waiters [ i ] == -1 )
                {
                        fprintf ( stderr , "Error : unable to create waiter #%d.\n" , i );
                        exit ( -1 );
                }
                else if ( waiters [ i ] == 0 )
                {
                        sprintf ( _waiterInstanceArgs [ 0 ] , "Waiter#%d" , i );
                        sprintf ( _waiterInstanceArgs [ 1 ] , "%d" , i );
                                  _waiterInstanceArgs [ 2 ] = NULL;

                        // Create new shm

                        ipckey = ftok ( KEY_FILE , me + 1 + i );
                        assert ( ipckey + 1 );
                        
                        r = execve ( _waiterInstanceName , _waiterInstanceArgs , envp );
                        switch ( errno )
                        {
                                case ENOENT: fprintf ( stderr , "Error : %s doesn't exist !\n" , _waiterInstanceName ); break;
                                case EACCES: fprintf ( stderr , "Error : cannot start %s, permission denied.\n" , _waiterInstanceName ); break;
                                case EPERM:  fprintf ( stderr , "Error : %s is not executable !\n" , _waiterInstanceName ); break;
                                case EISDIR: fprintf ( stderr , "Error : %s is a directory !\n" , _waiterInstanceName ); break;
                        }
                        exit ( -1 ); // Wimp out
                }
        }

        /* Main loop : client spawning. */

        fprintf ( stdout , "Fastfood is opened, clients may come in.\n" );

        while ( 1 )
        {
                /* Someone has the munchies ... \o/ */
                if ( nb_client ) /* There's a limitations to the client number to avoid both errors on fork and full message queue */
                {
                        nb_client--;
                        tmppid = fork ( );
                        if ( tmppid == -1 )
                        {
                                fprintf ( stderr , "Error : a customer failed to enter in the fastfood.\n" );
                                exit ( -1 );
                        }
                        else if ( tmppid == 0 )
                        {
                                srand ( getpid ( ) );
                                
                                /* Generating a random cooktime */
                                tmpcooktime = 1 + ( int ) ( 3.0 * ( rand ( ) / ( RAND_MAX + 1.0 ) ) );
                                sprintf ( _customerInstanceArgs [ 0 ] , "Customer#%d" , cusnum++ );
                                
                                sprintf ( _customerInstanceArgs [ 1 ] , "%d" , tmpcooktime );

                                for ( i = 0 ; i < fastfood . tools_c ; i++ )
                                {
                                        /* We're sure a command cannot requires more tools then cooks have. */
										tmptools_c = ( 42 + ( int ) ( 123345.0 * ( rand ( ) / ( RAND_MAX + 1.0 ) ) ) ) % ( tools [ i ] + 1 );
                                        sprintf ( _customerInstanceArgs [ i + 2 ] , "%d" , tmptools_c );
                                }
                                _customerInstanceArgs [ fastfood . tools_c + 2 ] = NULL;

                                execve ( _customerInstanceName , _customerInstanceArgs , envp );
                                exit ( -1 ); // Something's odd ...
                        }
                        customers = pidl_add ( customers , tmppid ); /* Linking the client into the list */
                }
                else
                        sleep ( 1 ); /* To limit client spawning to 1 per second max. */
        }

        /* Cleaning up stuff. ( -> atexit ) */
        exit ( 0 );
}
