#include "waiter.h"

/*


*/

waiter_t       *me       = NULL;
fastfood_t     *fastfood = NULL;
        
int main ( int argc , char * argv [ ] )
{
        int               msgqid;
        int               shmid;
        int               semid;
		key_t             key_code;
        FILE             *key_file;
        sigset_t          empty;
		sigset_t          masked;

        assert            ( argc >  1 ); /* Minimal test on args, program is supposed to be started by execve() */

        sigemptyset       ( & empty );
        sigemptyset       ( & masked );
        sigaddset         ( & masked , SIGUSR2 );

        sigprocmask       ( SIG_BLOCK , & masked , NULL ); /* Make sure SIGUSR2 is blocked during all execution */

        atexit            ( kill_the_waiter ); /* Make sure waiter is 'properly' exited if necessary */
        ssigaction        ( SIGUSR1 , exit );
        ssigaction        ( SIGUSR2 , handler );

        me = newwaiter    ( atoi ( argv [ 1 ] ) );

        key_file  = NULL;
        key_file  = fopen ( KEY_FILE , "r" );

        if ( ! key_file )
        {
                fprintf  ( stderr , "[waiter %d] Error : The file needed to compute the shm key doesn't exist !\n" , me -> id );
                exit ( -1 );
        }
        fclose ( key_file );

        key_code = ftok ( KEY_FILE , getppid ( ) );

        msgqid = msgget ( key_code, 0 );
        if ( msgqid < 0 ) 
        {
                fprintf ( stderr , "[waiter %d] Error : msgget returned -1 !\n" , me -> id );
                exit ( -1 );
        }
        
        shmid = shmget ( key_code , sizeof ( int ) , 0 );
        if ( shmid < 0 )
        {
                fprintf ( stderr , "[waiter %d] Error : shmid returned -1 !\n" , me -> id );
                exit ( -1 );
        }

        key_code = ftok ( KEY_FILE , getppid ( ) - 1 );

        semid = semget ( key_code , 1 , 0 );
        if ( semid < 0 ) 
        {
                fprintf ( stderr , "[cook %d] Error : semget returned -1 !\n" , me -> id );
                exit ( -1 );
        }

        fastfood      = shmat ( shmid , NULL, SHM_RDONLY );
        me -> nbtools = fastfood -> tools_c;

        fprintf ( stderr , "[waiter %d] Notice : I know there are %d tools !\n" , me -> id , me -> nbtools );

        while ( 1 )
        {
                fprintf ( stderr , "[waiter %d] Notice : Waiting .\n" , me -> id );
                getOrder ( me , msgqid );
                fprintf ( stderr , "[waiter %d] Notice : Order Acquired .\n" , me -> id );
                sendOrder ( me , msgqid );
                fprintf ( stderr , "[waiter %d] Notice : Cookers alerted .\n" , me -> id );
                waitForOrder ( msgqid , me );
                //fprintf ( stderr , "[waiter %d] Notice : Here you go !\n" , me -> id );
                makeClientPay ( me , semid );
                //fprintf ( stderr , "[waiter %d] Notice : See you !\n" , me -> id );
                clear_the_board ( me );
        }

        exit ( 0 ); /* This never supposed to be reached */
}

/*
        Called by atexit if waiter is terminated for some reason.
*/
void kill_the_waiter ( void )
{
        if ( me )
        {
                if ( me->task )
                {
                        free ( me -> task -> neededtools );
                        free ( me -> task );
                }        
                free ( me );
        }
        if ( fastfood )
                free ( fastfood );
        shmdt ( fastfood );
}

/*
        At the end at each order, waiter's task has to be reinitialised with this function.
*/
void clear_the_board ( waiter_t * me )
{
        free ( me -> task -> neededtools );
        me -> task -> neededtools = NULL;
        free ( me -> task );
        me -> task = NULL;
}

/*
        This allocates waiter's memory space ( called once, when program starts ).
*/
waiter_t * newwaiter( int id )
{
        waiter_t * out = malloc ( sizeof ( waiter_t ) );
        out -> id = id;
        out -> task = NULL;
        out -> waitingtobepaid = 0;
        return out;
}

/*
        This function waits for a order sent by a client. Waiter will be blocked until a message
        from a customer will be received.
*/
void getOrder ( waiter_t * me , int msqid )
{        
        struct msgbuf  anchor;
        char          *ptr;
        
        msgrcv ( msqid , & anchor , MAX_MSGQ_LENGTH * sizeof ( char ) , WAITER + me -> id , 0 );

        me -> task                = malloc ( sizeof ( order_t ) );
        me -> task -> neededtools = malloc ( sizeof ( int ) * me -> nbtools);
        ptr = anchor . content;

        memcpy ( & ( me -> task -> higher_pid ) , ptr , sizeof ( pid_t ) );
        ptr += sizeof ( pid_t );
        memcpy ( & ( me -> task -> eta ) , ptr , sizeof ( unsigned int ) );
        ptr += sizeof ( unsigned int );
        memcpy ( me -> task -> neededtools , ptr , sizeof ( int ) * me -> nbtools );
}

/*
        This function sends the order the waiters is dealing to a cook using a message queue.
*/
void sendOrder ( waiter_t * me , int msgqid )
{
        char          *ptr;
        struct msgbuf  anchor = { 0 , { '\0' } };
        int            myid   = me -> id;

        anchor . mtype = ORDER;

        ptr = ( anchor . content );

        memcpy ( ptr , & myid , sizeof ( int ) );
        ptr += sizeof ( int );
        memcpy ( ptr , & ( me -> task -> eta ) , sizeof ( unsigned int ) );
        ptr += sizeof ( unsigned int );
        memcpy ( ptr , me -> task -> neededtools , sizeof ( int ) * fastfood -> tools_c);
        ptr += sizeof ( int ) * fastfood -> tools_c;
       
        msgsnd ( msgqid , & anchor , MAX_MSGQ_LENGTH * sizeof ( char ) , 0 );
}

void waitForOrder ( int msgqid , waiter_t * me )
{     
        struct msgbuf order_completion = { 0 , { '\0' } };
        msgrcv ( msgqid , & order_completion , MAX_MSGQ_LENGTH * sizeof ( char ) , WAITER + me -> id , 0 );

        fprintf ( stderr , "[Waiter %d] Notice : The cook finished\n", me->id); 
}

void makeClientPay ( waiter_t * me, int semid )
{
        sigset_t      ensvide;
        struct sembuf take     = { 0 , - 1 , 0 };
        struct sembuf giveback = { 0 , + 1 , 0 };

        semop           ( semid , & take , 1 );
        sigemptyset     ( & ensvide );
        kill            ( me -> task -> higher_pid , SIGUSR2 );
        fprintf         ( stderr , "[Waiter %d] Notice : told the client he had to pay\n" , me -> id );

        me -> waitingtobepaid = 1;
        while ( me -> waitingtobepaid == 1 ) /* Let's wait for a SIGUSR2 */
                sigsuspend ( & ensvide );

        fprintf ( stderr , "[Waiter %d] Notice : client payed\n" , me -> id );
        semop   ( semid , & giveback , 1 );
}

void handler ( int s )
{
        s++;
        if ( me -> waitingtobepaid == 1 )
                me -> waitingtobepaid = 0;
}
