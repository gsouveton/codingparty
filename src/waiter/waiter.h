#ifndef _WAITER_H_
#define _WAITER_H_

#include "../global.h"
#include "../common.h"

        /* Creates a new waiter */
        waiter_t * newwaiter(int);

        /* Kills the waiter ( frees allocated memory ) */
        void kill_the_waiter ( void );

        /* Get an order from the message queue */
        void getOrder(waiter_t *,int);

        /* Send order to a cook */
        void sendOrder(waiter_t *,int);

        /* Waits for the cook to have finished cooking */
        void waitForOrder ( int , waiter_t *);

        /* Ask client to pay and wait for its payment back */
        void makeClientPay(waiter_t *, int semid);

        /* Used in response to signals */
        void handler(int s);

        /* Makes the waiter ready to take another order */
        void clear_the_board(waiter_t *);

#endif
