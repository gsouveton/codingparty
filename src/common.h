#ifndef _COMMON_H_
#define _COMMON_H_

#include "global.h"

// Safe free.
void ffree ( void * m );

// Free for linked list.
void lfree ( pid_lt * m );

/* add an item in the linked list*/
pid_lt * pidl_add ( pid_lt * pl , pid_t pid );

/* create an empty list*/
pid_lt * pidl_create ( void );
/* removes an item from the linked list*/
pid_lt * pidl_rm ( pid_lt * pl , pid_t pid );

/* set a sa_handler to a signal */
void ssigaction(int signal, void(*f)(int));
/* set a sigaction to a signal */
void ssahandler ( int signal, void (*f) (int, siginfo_t *, void *) );
/* pwd */
char * getInstanceDirectory ( void );
/*compute the full path of a program from its name*/
char * getInstancePath ( char * instance );
/*do nothing*/
void nop(int e);

#endif

