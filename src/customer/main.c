#include "customer.h"

fastfood_t        *fastfood = NULL;
customer_t        *me = NULL;
tool_t        *tools = NULL;

int main ( int argc , char * argv [ ] )
{
        FILE             *key_file;
        key_t             key_code;
        key_t             ipckey;
        ssize_t           i;
        int               cooktime;
        int               waiters;
        int               best_match;
        unsigned short    best;
        int               last_match;
        int               msqid;
        int               shm_fastfood;
        char             *ptr;
        sigset_t          empty;
        sigset_t          masked;
        struct msgbuf     anchor = { 0 , { '\0' } };
        pid_t             mypid = getpid ( );

        assert            ( argc > 2 ); /* Minimal test on args, program is supposed to be started by execve() */

        sigemptyset       ( & empty );
        sigemptyset       ( & masked );
        sigaddset         ( & masked , SIGUSR2 );

        sigprocmask       ( SIG_BLOCK , & masked , NULL ); /* Make sure SIGUSR2 is blocked during all execution */

        atexit            ( kick_the_customer ); /* Make sure waiter is 'properly' exited if necessary */
        ssigaction        ( SIGUSR1 , exit );
        ssahandler        ( SIGUSR2 , requete_payement  );

        me = newcustomer  ( );

        cooktime = atoi   ( argv [ 1 ] );        
        tools    = malloc ( sizeof ( int ) * ( argc - 2 ) );

        for ( i = 0 ; i < argc - 2 ; i++ )
                tools [ i ] = atoi ( argv [ i + 2 ] );

        key_file = NULL;
        key_file = fopen ( KEY_FILE , "r" );
        if ( ! key_file )
        {
                fprintf ( stderr , "[customer %d] Error : The file needed to compute the shm key doesn't exist !\n" , me->id );
                exit ( -1 );
        }
                fclose(key_file);
        ipckey = ftok ( KEY_FILE , getppid () );

        shm_fastfood = shmget ( ipckey , sizeof ( fastfood_t ) , 0 );
        if ( shm_fastfood == -1 )
        {
                fprintf ( stderr , "[customer %d] Error : Unable to access fastfood. !\n" , me->id );
                exit ( -1 );
        }
        fastfood = shmat ( shm_fastfood , NULL , SHM_RDONLY );

        // Looking for the waiter the more available.

        key_code = ftok ( KEY_FILE , getppid ( ) + 1 );

        waiters = semget ( key_code , fastfood -> waiters_c , 0 );

        best_match = 0 , best = semctl ( waiters , 0 , GETNCNT );;
        for ( i = 1 ; i < fastfood -> waiters_c ; i++ )
        {
                last_match = semctl ( waiters , i , GETNCNT );
                if ( last_match < best ) // >= to get a better distribution. (We're looking for the sem with the highest score).
                {
                        best = last_match;
                        best_match = i;
                }
        }
        
        struct sembuf call_waiter = { best_match , - 1 , 0 };
        struct sembuf free_waiter = { best_match , + 1 , 0 };

        semop ( waiters , &call_waiter , 1 );

        // The customer chooses what he wants : order creation.
        key_code = ftok ( KEY_FILE , getppid ( ) );

        msqid = msgget ( key_code , 0);

        if ( msqid < 0 )
        {
                fprintf ( stderr , "[customer %d] Error : msgget returned -1 !\n",me->id);
                exit ( -1 );
        }
        
        anchor . mtype = WAITER + best_match;
        
        ptr = (anchor.content);
        
        memcpy ( ptr , & mypid    , sizeof ( pid_t ) );
        ptr += sizeof ( pid_t );
        memcpy ( ptr , & cooktime, sizeof ( unsigned int ) );
        ptr += sizeof ( unsigned int );
        memcpy (ptr, tools, sizeof ( int ) * fastfood -> tools_c);
        ptr += sizeof ( int ) * fastfood -> tools_c;

        msgsnd ( msqid , & anchor , MAX_MSGQ_LENGTH * sizeof ( char ) , 0 );

        fprintf ( stderr , "[Customer %d] Notice : Might I see the bill ?\n" , me -> id );
        me -> mustpay = 1;
        while ( me -> mustpay == 1)
        {
                sigsuspend ( & empty ); /*wait for a SIGUSR2*/
        } 
        semop ( waiters , & free_waiter , 1 );

        fprintf ( stderr, "[Customer %d] Notice : Bye !\n" , me -> id );

        exit ( 0 ); // kick_the_customer is called here.
}

customer_t * newcustomer ( void )
{
        customer_t * out = malloc ( sizeof ( customer_t ) );
        out -> id = getpid( );
        out -> mustpay = 0;
        return out;        
}


void kick_the_customer ( void )
{

        free ( me );
        me = NULL;
        
        shmdt ( fastfood );
        fastfood = NULL;
        
        free ( tools );
        tools = NULL;
}


void requete_payement ( int s , siginfo_t * sig , void* v)
{
        fprintf ( stderr, "[Customer %d] Notice : I got asked to pay\n" , me -> id );

        if ( me -> mustpay )
        {        
                fprintf ( stderr , "[Customer %d] Notice : Payed\n" , me -> id );
                me -> mustpay = 0;
                kill ( sig -> si_pid , SIGUSR2);
        }        
}
