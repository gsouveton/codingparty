#ifndef _CUSTOMER_H_
#define _CUSTOMER_H_

#include "../global.h"

        /* Creates a new customer */
        customer_t * newcustomer ( void );

        /* Frees allocated memory */
        void kick_the_customer ( void );

        /* Asks waiter if it can pay and do it if able */
        void requete_payement ( int s , siginfo_t * sig , void * v );

#endif
