#include "common.h"

/********************************************************************/
/*                                                                  */
/* This file's content is uninteresting , please read 'common.h' :) */
/*                                                                  */
/********************************************************************/

// Free alias.
void ffree ( void * m )
{
	free ( m );
}

// Free for linked list.
void lfree ( pid_lt * m )
{
	if ( ! m )
		return;
	lfree ( m -> next );
	free ( m ); 
}

pid_lt * pidl_create ( void )
{
	return NULL;
}

pid_lt * pidl_add ( pid_lt * pl , pid_t pid )
{
	pid_lt * npl;
	npl = malloc ( sizeof ( pid_lt ) );
	npl -> pid = pid;
	npl -> next = pl;
	return npl;
}

pid_lt * pidl_rm ( pid_lt * pl , pid_t pid )
{
	if ( pl == NULL ) return NULL;
	if ( pl -> pid == pid )
	{
		pid_lt * nxt = pl->next;
		free(pl);
		return pidl_rm(nxt, pid);
	}
	else
	{
		pl->next=pidl_rm(pl->next, pid);
		return pl;
	}
}

void ssigaction ( int signal, void ( * f ) ( int ) )
{
        struct sigaction action;
        action . sa_handler = f;
        sigemptyset ( & action . sa_mask );
        action . sa_flags = 0;
        sigaction ( signal , & action , NULL );
}

void ssahandler ( int signal, void ( * f ) (int, siginfo_t *, void *) )
{
        struct sigaction action;
        action . sa_sigaction = f;
        sigemptyset ( & action . sa_mask );
        action . sa_flags = SA_SIGINFO;
        sigaction ( signal , & action , NULL );
}

char * getInstanceDirectory ( void )
{
	return getcwd ( NULL , 0 );
}

char * getInstancePath ( char * instance )
{
	char * path = malloc ( sizeof ( "./build/" ) + strlen ( instance ) + 1 );

	memcpy ( path , "./build/" , 8 );
	memcpy ( path + 8 , instance , strlen ( instance ) );

	return path;
	/*int r;
	char * d;
	char * path;
	struct stat * buf = NULL;
	size_t offset = 0;

	path = malloc ( strlen ( instance ) + strlen ( "./build/" ) );
	if ( ! path )
	{
		fprintf ( stderr , "Error : Unable to allocate memory.\n" );
		return NULL;
	}

	d = getInstanceDirectory ( );
	r = stat ( instance , buf );
	if ( r == -1 && errno == ENOENT )
	{
		r = chdir ( "./build/" );
		if ( r == -1 && errno == EACCES )
		{
			fprintf ( stderr , "Error : Unable to find program : %s.\n" , instance );
			return NULL;
		}
		memcpy ( path , "./build/" , 8 );
		offset = 8;
	}
	memcpy ( path , instance , offset ); // instance ends with a '\0'.
*/
	// Debug
	//fprintf ( stdout , "path : %s\n" , path );

	return path;
}

void nop (int e){}
