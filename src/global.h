#ifndef _GLOBAL_H_
#define _GLOBAL_H_

#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <errno.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <sys/msg.h>
#include <sys/stat.h>
#include <signal.h>
#include <sys/types.h> 

#define MAX_MSGQ_LENGTH           64
#define KEY_FILE          "key.serv"

typedef struct pid_ls pid_lt;
struct pid_ls
{
        pid_t     pid;
        pid_lt   *next;
};

struct msgbuf
{
        long           mtype;
        char               content [ MAX_MSGQ_LENGTH ];
};

typedef int tool_t;

/**
        That's an order from a client to a waiter or from a waiter to a cook
**/
struct order_s
{
        int * neededtools;
        unsigned int eta;
        pid_t higher_pid;
};
typedef struct order_s order_t;

/**
        a waiter
**/
struct waiter_s
{
        int id; 
        int nbtools;
        order_t * task;
        int waitingtobepaid;
};
typedef struct waiter_s waiter_t;

/**
        a cook
**/
struct cook_s
{
        int id;
        int nbtools;
        order_t * task;
};
typedef struct cook_s cook_t;

/**
        a customer
**/
struct customer_s
{
        int id;
        int mustpay;
};
typedef struct customer_s customer_t;

enum msg_e { ORDER = 1 , WAITER = 100 };

struct  fastfood_s
{
        int cooks_c;
        int waiters_c;
        int terms_c;
        int tools_c;
};
typedef struct fastfood_s fastfood_t;

#include "common.h"

#endif
